# Forge Issue Translation

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE)

This is an example [Forge](https://developer.atlassian.com/platform/forge/) app that translates Jira issue fields contents into a range of different languages using the [Azure Translator Text API](https://docs.microsoft.com/en-us/azure/cognitive-services/translator/). 

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository
2. Run `forge register` to register a new copy of this app to your developer account
3. Run `npm install` to install your dependencies
4. Run `forge deploy` to deploy the app into the default environment
5. Run `forge install` and follow the prompts to install the app

## Usage

Press the corresponding button on the issue view to translate the issue summary and description into the language of your choice.

![Animation of translate issue content panel](./demo.gif)

This app is designed as a Forge reference example for developers. There are no user-facing configuration options, but you can modify the supported languages and translated fields by editing `src/index.jsx`.

## Installation

1. [Sign up for the Azure Translator Text API](https://docs.microsoft.com/en-us/azure/cognitive-services/translator/translator-text-how-to-signup) (the free tier allows 2 million characters per month at time of writing)
2. Find your **Authentication key** and **Location** on the "Keys and Endpoint" page.
3. Set an encrypted [environment variable](https://developer.atlassian.com/platform/forge/environments/) keyed by `TRANSLATE_API_KEY` with a value of your Translator Text API **Authentication key**. `forge variables set --encrypt TRANSLATE_API_KEY xxxxxxxxxx`.
4. Set the `TRANSLATE_API_LOCATION` environment variable to the value of your Translator Text API **Location**. `forge variables set TRANSLATE_API_LOCATION xxxxxxxxxx`.
5. Run `forge deploy` to deploy the changes to your environment variables.
6. You're done! Test out the app by browsing to a Jira issue and clicking one of the buttons on the "Translate" panel.

## Debugging

You can enable verbose logging by setting the `DEBUG_LOGGING` [environment variable](https://developer.atlassian.com/platform/forge/environments/) to `1`. Logs can then be viewed with the `forge logs` command.

Alternatively, you can use the [`forge tunnel`](https://developer.atlassian.com/platform/forge/change-the-frontend-with-forge-ui/#set-up-tunneling) command to run your Forge app locally. Note that you must pass the environment variable values to the tunnel with the prefix `FORGE_USER_VAR_`, e.g.:

```
FORGE_USER_VAR_TRANSLATE_API_KEY=your_translate_api_key_here FORGE_USER_VAR_DEBUG_LOGGING=1 forge tunnel
```

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![From Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers.png)](https://www.atlassian.com)
